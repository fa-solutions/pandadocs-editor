let FAClient = null;
let recordId = null;

const SERVICE = {
    name: 'FreeAgentService',
    appletId: `aHR0cHM6Ly9mYS1zb2x1dGlvbnMuZ2l0bGFiLmlvL3BhbmRhZG9jcy1lZGl0b3Iv`,
};

let quoteInfo = {
    entity: "quote",
    id: "",
    field_values: {}
};

function startupService() {
    FAClient = new FAAppletClient({
        appletId: SERVICE.appletId,
    });

    FAClient.on("openAllDocs", (data) => {
        document.querySelector('#pandadoc-sdk').innerHTML = '';
        FAClient.open();
        var doclist = new PandaDoc.DocList({mode: PandaDoc.DOC_LIST_MODE.LIST});
        doclist.init({
            el: '#pandadoc-sdk',
            data: {},
            events: {
                onInit: function(){},
                onDocumentCreate: function(){}
            }
        });
        console.log(data);
    });

    FAClient.on("openPandaEditor", (data) => {
        console.log(data);
        document.querySelector('#pandadoc-sdk').innerHTML = '';
        FAClient.open();
        let quote = data.quote;
        let fieldValues = quote.field_values;
        let queteId = fieldValues.seq_id.value || '';
        let account = fieldValues.quote_field1.display_value || '';
        let startDate = fieldValues.quote_field10.display_value || '';
        let endDate = fieldValues.quote_field11.display_value || '';
        let address = fieldValues.quote_field29.display_value || '';
        let email = fieldValues.quote_field27.display_value || '';
        let recipients = [];
        if (email) {
            recipients.push({email})
        };
        let status = fieldValues.quote_field33 && fieldValues.quote_field33.display_value ? fieldValues.quote_field33.display_value : null;
        if (!status || status === 'Not Created') {

            FAClient.listEntityValues(
                {
                    entity: "engine",
                    filters: [
                        {
                            field_name: "parent_entity_reference_id",
                            operator: "includes",
                            values: [quote.id],
                        },
                    ],
                },
                (lines) => {
                    console.log(lines);
                    openPandaDoc(lines);
                });

            function openPandaDoc(lines=null) {
                console.log(lines)
                FAClient.open();

                let items = [];
                lines.map(line => {
                    let lineFields = line.field_values;
                    items.push({
                        name: lineFields.engine_field0.display_value,
                        price: lineFields.engine_field2.value,
                        qty: lineFields.engine_field1.value,
                    })
                });

                var editor = new PandaDoc.DocEditor();
                editor.show({
                    el: '#pandadoc-sdk',
                    data: {
                        docName: `FA Quote #${queteId}`,
                        recipients,
                        tokens: {
                            "Client.Company": account,
                            "Quote.No": queteId,
                            "Quote Start": startDate,
                            "Quote End": endDate,
                            "Client.Address":address
                        },
                        metadata: { quote: quote.id },
                        items: items,
                    },
                    events: {
                        onInit: function () {
                            console.log('init')
                        },
                        onDocumentCreated: function (data) {
                            quoteInfo.field_values = {
                                quote_field33: "Draft",
                                quote_field36: data.document.name,
                                quote_field34: data.document.id,
                                quote_field35: data.document.url.replace('#', '/a/#')
                            }
                            quoteInfo.id = quote.id;
                            FAClient.updateEntity(quoteInfo,(quote) => {
                                console.log(quote);
                                console.log('created')
                            });
                        },
                        onDocumentSent: function () {
                            quoteInfo.field_values = { quote_field33: "Sent" }
                            quoteInfo.id = quote.id;
                            FAClient.updateEntity(quoteInfo,(quote) => {
                                console.log(quote);
                                console.log('sent');
                            });
                        },
                        onClose: function () {
                            quoteInfo.id = quote.id;
                            quoteInfo.field_values = { quote_field33: "Closed" };
                            FAClient.updateEntity(quoteInfo,(quote) => {
                                console.log(quote);
                                console.log('closed');
                            });
                        }
                    }
                });
            }
        } else {
            document.querySelector('#pandadoc-sdk').innerHTML = '';
            FAClient.open();
            var doclist = new PandaDoc.DocList({mode: PandaDoc.DOC_LIST_MODE.LIST});
            doclist.init({
                el: '#pandadoc-sdk',
                data: {
                    metadata: { quote: quote.id }
                },
                events: {
                    onInit: function(){},
                    onDocumentCreate: function(){}
                }
            });
        }
    });
}
